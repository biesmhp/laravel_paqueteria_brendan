<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transportista;
use Illuminate\Support\Facades\DB;

class TransportistaController extends Controller
{
    //
    public function index()
    {
		$transportistas=DB::table('transportistas')->get();
        return view('transportistas.index',['transportista'=>$transportistas]);
    }

    public function show(Transportista $transportista)
    {
		//print_r($transportista->slug);
		//$transportistaid=DB::table('transportistas')->get('slug');
        return view('transportistas.show', ['transportista'=>$transportista]);
    }

    public function create() {
        return view('transportistas.create');
    }

    public function edit(Transportista $transportista) {
		//$transportistaid=DB::table('transportistas')->get($transportista);
        return view('transportistas.edit', ['transportista'=>$transportista]);
    }
}
