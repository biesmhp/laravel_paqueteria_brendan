@extends('layouts.master')
@section('title')
    Transportistas
@endsection
@section('contenido')
    <div class="row">
        @foreach( $transportista as $clave => $transportist)
        <div class="col-xs-12col-sm-6col-md-4">
            <a href="{{ route('transportistas.show' , $transportista[$clave]->slug) }}">
                <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportist->imagen}}" style="height:200px" alt="{{$transportist->imagen}}"/>
                <h4 style="min-height:45px;margin:5px 0 10px 0">
                    {{$transportist->nombre}}

                </h4>
            </a>
        </div>
        @endforeach
    </div>
@endsection