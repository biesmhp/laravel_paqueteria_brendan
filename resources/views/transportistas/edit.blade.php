@extends('layouts.master')
@section('title')
    Zoológico
@endsection
@section('contenido')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Editar animal: {{$animal['especie']}}</div>
                <div class="card-body" style="padding:30px">
                {{--TODO: Abrir el formulario e indicar el método POST --}}{{--TODO: Protección contra CSRF --}}
                <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="especie">Especie</label>
                        <input type="text" name="especie" id="especie" class="form-control" required>
                    </div>
                    <div class="form-group">
                        {{--TODO: Completa el inputpara el peso--}}
                        <label for="peso">Peso</label>
                        <input type="text" name="peso" id="peso" class="form-control" required>
                    <br>
                    </div>
                    <div class="form-group">
                        {{--TODO: Completa el input para la altura--}}
                        <label for="altura">Altura</label>
                        <input type="text" name="altura" id="altura" class="form-control" required>
                    </div>
                    <div class="form-group">
                        {{--TODO: Completa el input para la fecha de nacimiento --}}
                        <label for="fechaNac">Fecha de nacimiento</label>
                        <input type="text" name="fechaNac" id="fechaNac" class="form-control" required>
                    </div>
                    <div class="form-group">
                        {{--TODO: Completa el input para la alimentación--}}
                        <label for="alimentacion">Alimentacion</label>
                        <input type="text" name="alimentacion" id="alimentacion" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>
                    </div><div class="form-group">
                        {{--TODO: Completa el input para la imagen --}}
                        <label for="imagen">Imagen</label>
                        <figure>
                            <img src="{{asset('assets/images')}}/{{$animal['imagen']}}">
                        </figure>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar animal</button>
                    </div>
                {{--TODO: Cerrar formulario --}}
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection