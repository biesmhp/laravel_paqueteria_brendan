@extends('layouts.master')
@section('title')
    Transportistas
@endsection
@section('contenido')
    <div class="row">
        <div class="col-sm-3">
            @php
                print_r($transportista);
                
            @endphp
            <figure>
                <img src="{{asset('assets/imagenes/transportistas')}}/{{$transportista->imagen}}" style="height:200px" alt="{{$transportista->imagen}}"/>
                <figcaption>Pie de foto: {{$transportista->nombre}} </figcaption>
            </figure>
        </div>
        <div class="col-sm-9">
            <h1>{{$transportista->nombre}} ({{$transportista['fechaPermisoConducir']}})</h1>
            
            <footer>
                {{-- <a href="{{ route('transportistas.edit' , $transportista->slug) }}" class="btn btn-primary">Editar</a> --}}
                <a href="{{ route('transportistas.index')}}" class="btn btn-secondary">Listado de transportistas</a>
            </footer>
        </div>
    </div>
@endsection