<?php

namespace Database\Factories;

use App\Models\Transportista;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransportistaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transportista::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
