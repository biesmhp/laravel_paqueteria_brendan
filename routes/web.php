<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransportistaController;
use App\Http\Controllers\InicioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [InicioController::class,'inicio'])->name('home');

Route::get('Transportistas', [TransportistaController::class,'index'])->name('transportistas.index');

Route::get('Transportistas/{Transportista}', [TransportistaController::class,'show'])->name('transportistas.show');

Route::get('Transportistas/crear', [TransportistaController::class,'create'])->name('transportistas.create');

Route::get('Transportistas/{Transportista}/editar', [TransportistaController::class,'edit'])->name('transportistas.edit');
